package com.bhinary.terra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan
@EnableJpaRepositories("com.bhinary.terra.repositories")
public class TerraApp1Application {

	public static void main(String[] args) {
		SpringApplication.run(TerraApp1Application.class, args);
	}

}
