package com.bhinary.terra.component;

import javax.mail.MessagingException;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.bhinary.terra.config.SiteConfiguration;

@Component
public class MailHelper {

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	SiteConfiguration settings;
	
	/**
	 * Send Mail Helper
	 * @param subject
	 * @param body
	 * @param targetEmail
	 * @return
	 */
	public boolean sendEmail(String subject, String body, String targetEmail) {
		boolean success = false;
		
		MimeMessage mail = javaMailSender.createMimeMessage();
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(targetEmail);
			helper.setReplyTo(settings.sourceEmail);
			helper.setFrom(settings.sourceEmail);
			helper.setSubject(subject);
			helper.setText(body, true);
			javaMailSender.send(mail);
			success = true;			
		} catch (MessagingException e) {
			e.printStackTrace();
			success = false;
		}	
		return success;
	}
	
	
}