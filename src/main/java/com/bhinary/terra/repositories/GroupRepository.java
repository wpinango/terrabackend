package com.bhinary.terra.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bhinary.terra.entities.GroupEntity;

@Repository
public interface GroupRepository extends CrudRepository<GroupEntity, Integer>{
	
	public GroupEntity findById(int id);
	
	

}
