package com.bhinary.terra.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bhinary.terra.entities.*;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer>{

	//public UserEntity findByUserName(String name);
	
	public UserEntity findByRole(int role);
	
	public UserEntity findById(int id);
	
	public UserEntity findByEmail(String email);
	
	public UserEntity findByPhone(String phone);
	
	@Query(value = "SELECT * FROM users where role = 1", nativeQuery = true)
	public Iterable<UserEntity> findAllByOrderByNameAsc();
	
	//@Query(value = "SELEC * FROM users where session=?1", nativeQuery = true)
	public UserEntity findBySession(String session);
	
	//@Query("SELECT * FROM users where role = 1")
	//public List<UserEntity>getAppUsers();
	//@Query("select new UserEntity(s.userName, s.identityNumber, s.phone, s.email, s.address) from UserEntity s where s.id=?1")
	//public UserEntity getUser(int userId);
}
