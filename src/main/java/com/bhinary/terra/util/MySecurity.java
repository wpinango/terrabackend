package com.bhinary.terra.util;

import org.mindrot.jbcrypt.BCrypt;

public class MySecurity {
	
	private final static int workload = 12;

	public static String hashPassword(String password_plaintext) {
        String salt = BCrypt.gensalt(workload);
        String hashed_password = BCrypt.hashpw(password_plaintext, salt);

        return (hashed_password);
    }
	
	 public static boolean checkPassword(String password_plaintext, String stored_hash) {
	        boolean password_verified = false;

	       
	        if (null == stored_hash) {
	            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");
	        }
	        
	        if (  !stored_hash.startsWith("$2a$") && stored_hash.startsWith("$2y$")) {
	        	String nHash = stored_hash.replace("$2y$", "$2a$");
	        	System.out.println("old hash: "+stored_hash);
	        	System.out.println("new hash: "+nHash);
	        	stored_hash = nHash;
	        }

	        password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

	        return (password_verified);
	    }
}
