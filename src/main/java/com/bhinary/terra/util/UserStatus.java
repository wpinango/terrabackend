package com.bhinary.terra.util;

public class UserStatus {
	
	public static final int REGISTERED = 0;
	
	public static final int CONFIRMED = 1;
	
	public static final int READY = 2;
}
