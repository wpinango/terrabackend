package com.bhinary.terra.util;

import java.util.Random;

public class PasswordGeneratorHelper {

	private static final String dCase = "abcdefghijklmnopqrstuvwxyz";
    private static final String uCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String intChar = "0123456789";
    private static Random r = new Random();
    private static String pass = "";

    public static String getRandomPassword() {
    	String password = "";
        while (password.length () != 6){
            int rPick = r.nextInt(4);
            if (rPick == 0){
                int spot = r.nextInt(25);
                password += dCase.charAt(spot);
            } else if (rPick == 1) {
                int spot = r.nextInt (25);
                password += uCase.charAt(spot);
            } else if (rPick == 2) {
                int spot = r.nextInt (7);
                password += intChar.charAt (spot);
            }
        }
        return password;
    }
    
    
    public static void main(String[] args) {
    	System.out.println(getRandomPassword());
    }
}
