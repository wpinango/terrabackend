package com.bhinary.terra.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "groups_table")
public class GroupEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	@Column(name = "group_name")
	@NotNull
	private String groupName;

	@NotNull
	@Column(name = "group_type")
	private int groupType;
	
	@Column(name = "active")
	private Integer active;
	
	public GroupEntity() {
		
	}
	
	public GroupEntity(int id, String groupName, int groupType) {
		this.id = id;
		this.groupName = groupName;
		this.groupType = groupType;
	}


	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getGroupType() {
		return groupType;
	}

	public void setGroupType(int groupType) {
		this.groupType = groupType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
	
	
	
}
