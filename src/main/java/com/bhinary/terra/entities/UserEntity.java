package com.bhinary.terra.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.google.gson.annotations.Expose;



@Entity
@Table(name = "users", uniqueConstraints = {
		@UniqueConstraint(
				   name = "email_constraint",columnNames = {"email"}
			),
})
public class UserEntity {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "role")
	private Integer role;
	
	@NotEmpty
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	@NotEmpty
	@JsonProperty(access = Access.WRITE_ONLY)
	//@Pattern(regexp = Global.PASSWORD_REGEXP)
	private String password;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "attemps")
	private int attempts;
	
	@Column(name = "session")
	@JsonIgnore
	private String session;
	
	@Column(name = "status")
	//@NotNull
	@Min(value = 0l)
	@Max(value = 2l)
	private Integer status;
	
	@Column(name = "active")
	private Integer active;
	
	@Column(name = "temporal_password")
	private Boolean temporalPassword;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	@Column(name = "updated_at")
	@Temporal(TemporalType.DATE)
	private Date updatedAt;
	
	@Column(name = "phone")
	@NotEmpty
	private String phone;
	
	@Column(name = "name")
	private String name; 
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "ip")
	@Expose(serialize = false, deserialize = true) 
	private String ip;
	
	@Column(name = "job_title")
	private String jobTitle;
	
	@Column(name = "group_id")
	private int groupId;
	
	
	public UserEntity() {
		
	}
	
	
	public UserEntity(String email, String phone, String address, Integer role) {
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.role = role;
		
	}
	
	public UserEntity(String email, String identityNumber, String phone, String password,
			String address, Integer role, Integer attempts, String session, Integer status, Integer active,
			Boolean temporalPassword, Date createdAt, Date updatedAt, String jobTitle) {
		super();
		this.email = email;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.address = address;
		this.role = role;
		this.attempts = attempts;
		this.session = session;
		this.status = status;
		this.active = active;
		this.temporalPassword = temporalPassword;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.jobTitle = jobTitle;
	}
	
	
	public void setIp(String ipAddress) {
		this.ip = ipAddress;
	}
	
	public String getIp() {
		return ip;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getAttempts() {
		return attempts;
	}

	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Boolean getTemporalPassword() {
		return temporalPassword;
	}

	public void setTemporalPassword(Boolean temporalPassword) {
		this.temporalPassword = temporalPassword;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	public String getJobTitle() {
		return this.jobTitle;
	}
	
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	
	public int getGroupId() {
		return groupId;
	}
	
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
	//INSERT INTO users VALUES (0, 'ADMIN', 'admin@terrahydrochem.com', '123456','New Braunfels', 0, 0, '', '+584149913127', '2019-11-20 22:15:30', '2019-11-20 22:15:30', 'admin', 'admin', '');
	
}