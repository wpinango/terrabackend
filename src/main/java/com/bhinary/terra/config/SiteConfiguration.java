package com.bhinary.terra.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SiteConfiguration {

	@Value("${server.port}")
	public int applicationPort;
	
	@Value("${spring.mail.username}")
	public String sourceEmail;

}
