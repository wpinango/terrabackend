package com.bhinary.terra.transport;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.bhinary.terra.globals.Global;

public class PasswordRequest {
	@Column(name = "password")
	@NotEmpty
	@Pattern(regexp = Global.PASSWORD_REGEXP)
	public String password;
	
	@Column(name = "confirm_password")
	@NotEmpty
	@Pattern(regexp = Global.PASSWORD_REGEXP)	
	public String confirmPassword;
	
	protected PasswordRequest(){}

	public PasswordRequest(String password, String confirmPassword) {
		super();
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
