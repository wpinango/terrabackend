package com.bhinary.terra.transport;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class EmailRequest {
	@NotEmpty
	@Email
	private String email;
	
	protected EmailRequest(){}

	public EmailRequest(String email) {
		super();
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
