package com.bhinary.terra.controllers;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bhinary.terra.component.MailHelper;
import com.bhinary.terra.entities.UserEntity;
import com.bhinary.terra.globals.Global;
import com.bhinary.terra.models.AttemptError;
import com.bhinary.terra.repositories.UserRepository;
import com.bhinary.terra.response.BaseResponse;
import com.bhinary.terra.response.OkResponse;
import com.bhinary.terra.response.UserExistMessage;
import com.bhinary.terra.transport.EmailRequest;
import com.bhinary.terra.transport.PasswordRequest;
import com.bhinary.terra.util.MySecurity;
import com.bhinary.terra.util.PasswordGeneratorHelper;
import com.bhinary.terra.util.UserActive;
import com.bhinary.terra.util.UserStatus;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/users")

public class UserController {
	public static final String WRONG_FORMAT = "WRONG-FORMAT";
	public static final String WRONG_CREDENTIALS = "WRONG-CREDENTIALS";
	public static final String NOT_FOUND = "NOT-FOUND";
	public static final String USER_NOT_FOUND = "USER-NOT-FOUND";
	public static final String USER_ALREADY_ACTIVE = "USER-ALREADY-ACTIVE";
	public static final String INTERNAL_SERVER_ERROR = "INTERNAL-SERVER-ERROR";
	public static final String BAD_SESSION = "BAD-SESSION";
	public static final String BLOCKED_ACCOUNT = "BLOCKED-ACCOUNT";
	public static final String PASSWORD_NOT_MATCH = "PASSWORD-NOT_MATCH";
	public static final String USER_UNATHORIZED = "UNATHORIZED";
	
	@Autowired
	private MailHelper mailHelper;
	
	@Autowired
	UserRepository userRep;
	
	@RequestMapping(value = "/alogin", method = RequestMethod.POST, produces = "application/json") 
	public ResponseEntity<OkResponse> adminLogin(@RequestHeader Map<String, String> headers, 
			HttpServletResponse responseHeaders, @RequestHeader("Authorization") String auth) {
		OkResponse response = new OkResponse();
		String newToken;
		String[] authArray = auth.split(" ");
		if (authArray.length > 2) {
			response.setErrorResponse(BAD_SESSION, null); 
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		byte[] bytes = null;
		try {
			bytes = authArray[1].getBytes("UTF-8");
		} catch (Exception ex) {
			bytes = null;
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		byte[] valueDecoded = Base64.getDecoder().decode(bytes);
		String readable = new String(valueDecoded);
		String[] accessData = readable.split(":");
		if (accessData == null) {
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		if (accessData.length < 2) {
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		String userEmail = accessData[0];
		System.out.println(userEmail + ' ' + accessData[1]);
		if (!userEmail.matches(Global.VALID_EMAIL_FORMAT)) {
			response.setErrorResponse(WRONG_FORMAT, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		UserEntity currentUser = userRep.findByEmail(userEmail);
		
		if (currentUser == null) {
			response.setErrorResponse(USER_NOT_FOUND, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		if (currentUser.getRole() != 0) {
			response.setErrorResponse(USER_NOT_FOUND, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		System.out.println(userEmail + " + " + accessData[1] + " " + currentUser.getEmail() + " " + currentUser.getPassword());
		
		if (MySecurity.checkPassword(accessData[1], currentUser.getPassword())) {
			newToken = MySecurity.hashPassword(accessData[1]);
			currentUser.setSession(newToken);
			userRep.save(currentUser);
		} else {
			int attempts = currentUser.getAttempts();
			if (attempts < Global.MAX_ATTEMPS) {
				attempts++;
				currentUser.setAttempts(attempts);
				if (attempts == Global.MAX_ATTEMPS) {
					currentUser.setSession("");
					currentUser.setActive(UserActive.INACTIVE);
				}
				userRep.save(currentUser);
				AttemptError error = new AttemptError();
				error.setAttempts(currentUser.getAttempts()); 
				response.setErrorResponse(WRONG_CREDENTIALS, error);
				return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
			} else {
				response.setErrorResponse(BLOCKED_ACCOUNT, null);
				return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
			}
		}
		
		currentUser.setAttempts(0);
		UserEntity resultLoginUser = userRep.save(currentUser);
		if (resultLoginUser == null) {
			response.setErrorResponse(INTERNAL_SERVER_ERROR, null);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		responseHeaders.setHeader(Global.X_IDENTITY, currentUser.getId().toString());
		responseHeaders.setHeader(Global.X_SESSION, newToken);
		responseHeaders.setHeader(Global.X_ROLE, currentUser.getRole().toString());
		//
		resultLoginUser.setPassword(null);
		resultLoginUser.setSession(null);
		//resultLoginUser.setRole(null);
		response.setOkResponse(resultLoginUser);

		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json") 
	public ResponseEntity<OkResponse> login(@RequestHeader Map<String, String> headers, 
			HttpServletResponse responseHeaders, @RequestHeader("Authorization") String auth) {
		OkResponse response = new OkResponse();
		String newToken;
		String[] authArray = auth.split(" ");
		if (authArray.length > 2) {
			response.setErrorResponse(BAD_SESSION, null); 
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		byte[] bytes = null;
		try {
			bytes = authArray[1].getBytes("UTF-8");
		} catch (Exception ex) {
			bytes = null;
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		byte[] valueDecoded = Base64.getDecoder().decode(bytes);
		String readable = new String(valueDecoded);
		String[] accessData = readable.split(":");
		if (accessData == null) {
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		if (accessData.length < 2) {
			response.setErrorResponse(BAD_SESSION, null);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		String userEmail = accessData[0];
		System.out.println(userEmail + ' ' + accessData[1]);
		if (!userEmail.matches(Global.VALID_EMAIL_FORMAT)) {
			response.setErrorResponse(WRONG_FORMAT, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		UserEntity currentUser = userRep.findByEmail(userEmail);
		
		if (currentUser == null) {
			response.setErrorResponse(USER_NOT_FOUND, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		if (currentUser.getRole() == 0) {
			response.setErrorResponse(USER_NOT_FOUND, null);
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		System.out.println(userEmail + " + " + accessData[1] + " " + currentUser.getEmail() + " " + currentUser.getPassword());
		
		currentUser.setActive(UserActive.ACTIVE);
		
		if (MySecurity.checkPassword(accessData[1], currentUser.getPassword())) {
			newToken = MySecurity.hashPassword(accessData[1]);
			currentUser.setSession(newToken);
			userRep.save(currentUser);
		} else {
			int attempts = currentUser.getAttempts();
			if (attempts < Global.MAX_ATTEMPS) {
				attempts++;
				currentUser.setAttempts(attempts);
				if (attempts == Global.MAX_ATTEMPS) {
					currentUser.setSession("");
					currentUser.setActive(UserActive.INACTIVE);
				}
				userRep.save(currentUser);
				AttemptError error = new AttemptError();
				error.setAttempts(currentUser.getAttempts()); 
				response.setErrorResponse(WRONG_CREDENTIALS, error);
				return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
			} else {
				response.setErrorResponse(BLOCKED_ACCOUNT, null);
				return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
			}
		}
		
		currentUser.setAttempts(0);
		UserEntity resultLoginUser = userRep.save(currentUser);
		if (resultLoginUser == null) {
			response.setErrorResponse(INTERNAL_SERVER_ERROR, null);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		responseHeaders.setHeader(Global.X_IDENTITY, currentUser.getId().toString());
		responseHeaders.setHeader(Global.X_SESSION, newToken);
		responseHeaders.setHeader(Global.X_ROLE, currentUser.getRole().toString());
		//
		resultLoginUser.setPassword(null);
		resultLoginUser.setSession(null);
		//resultLoginUser.setRole(null);
		response.setOkResponse(resultLoginUser);

		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<BaseResponse> registerUser(@Valid @RequestBody UserEntity user,  HttpServletRequest request) {
		Date date = new Date();

		UserEntity resultUser = userRep.findByEmail(user.getEmail());
		if (resultUser != null) {
			return new ResponseEntity<BaseResponse>(new UserExistMessage(), HttpStatus.CONFLICT);
		}
		
		System.out.println("valores : " + new Gson().toJson(user));
		
		user.setPassword(PasswordGeneratorHelper.getRandomPassword());
		
		String newPassword = MySecurity.hashPassword(user.getPassword());
		String nHash = newPassword.replace("$2a$", "$2y$");
		user.setAttempts(0);
		user.setSession("");
		user.setPassword(nHash);
		user.setCreatedAt(date);
		user.setUpdatedAt(date);
		user.setActive(UserActive.INACTIVE);
		user.setStatus(UserStatus.REGISTERED);
		//user.setTemporalPassword(false);
		user.setIp(request.getRemoteAddr());

		UserEntity result = userRep.save(user);
		OkResponse response = new OkResponse();

		// result.setPassword(null);

		response.setOkResponse("OK");
		/////
		// Se envia el correo con el enlace de activacion

		boolean success = false;

		/* MimeMessage mail = javaMailSender.createMimeMessage(); */
		String targetEmail = result.getEmail();


		success = mailHelper.sendEmail("Welcome to TerraHydroChem ", "Your temporal password is 123456 and will be valid for 3 days.", targetEmail);
		if (success) {
			response.setOkResponse("OK");
		} else {
			response.setErrorResponse("EMAIL-NOT-SENT", null);
		}

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit_user", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<BaseResponse> editUserData(@Valid @RequestBody UserEntity user,  HttpServletRequest request) {
		Date date = new Date();

		OkResponse response = new OkResponse();
		
		UserEntity resultUser = userRep.findByEmail(user.getEmail());
		if (resultUser == null) {
			response.setErrorResponse("EMAIL-NOT-FOUND", null);
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
		//System.out.println("valores : " + new Gson().toJson(user));
		
		//user.setPassword(PasswordGeneratorHelper.getRandomPassword());
		
		//String newPassword = MySecurity.hashPassword(user.getPassword());
		//String nHash = newPassword.replace("$2a$", "$2y$");
		//resultUser.setAttempts(0);
		//user.setSession("");
		//user.setPassword(nHash);
		//resultUser.setCreatedAt(date);
		resultUser.setName(user.getName());
		resultUser.setLastName(user.getLastName());
		resultUser.setJobTitle(user.getJobTitle());
		resultUser.setPhone(user.getPhone());
		resultUser.setAddress(user.getAddress());
		resultUser.setGroupId(user.getGroupId());
		resultUser.setUpdatedAt(date);
		resultUser.setActive(user.getActive());
		resultUser.setStatus(UserStatus.CONFIRMED);
		//user.setTemporalPassword(false);
		resultUser.setIp(request.getRemoteAddr());

		UserEntity result = userRep.save(resultUser);

		// result.setPassword(null);

		response.setOkResponse("OK");

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reset_password", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<OkResponse> resetPassword(@Valid @RequestBody EmailRequest emailRequest) {
		OkResponse response = new OkResponse();

		String targetEmail = emailRequest.getEmail();

		/*
		 * if (!targetEmail.matches(Global.IDENTITY_NUMBER_REGEXP)) {
		 * response.setErrorResponse("BAD-EMAIL-FORMAT"); return new
		 * ResponseEntity<>(response, HttpStatus.BAD_REQUEST); }
		 */

		UserEntity targetUser = userRep.findByEmail(targetEmail);

		if (targetUser == null) {
			response.setErrorResponse("EMAIL-NOT-FOUND", null);
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}

		boolean success = false;

		String randomPasswordPlain = PasswordGeneratorHelper.getRandomPassword();

		String randomTokenPlain = PasswordGeneratorHelper.getRandomPassword();

		String newPasswordHash = MySecurity.hashPassword(randomPasswordPlain);

		String newTokenHash = MySecurity.hashPassword(randomTokenPlain);

		targetUser.setPassword(newPasswordHash);
		targetUser.setSession(newTokenHash);
		targetUser.setAttempts(0);
		targetUser.setTemporalPassword(true);

		UserEntity result = userRep.save(targetUser);
		if (result == null) {
			response.setErrorResponse("INTERNAL-SERVER-ERROR", null);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		success = mailHelper.sendEmail("Su clave ha sido reseteada - TerraHydroChem",
				"Esta es su nueva clave <b>" + randomPasswordPlain + "</b>", targetEmail);

		if (!success) {
			response.setErrorResponse("EMAIL-SENDER-ERROR", null);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.setOkResponse("OK");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/change_password", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<OkResponse> updatePassword(@RequestHeader(Global.X_SESSION) String session,
			@RequestHeader(Global.X_IDENTITY) int xIdentity, @Valid @RequestBody PasswordRequest password) {

		OkResponse response = new OkResponse();

		UserEntity targetUser = userRep.findById(xIdentity);
		if (targetUser == null) {
			response.setErrorResponse(USER_NOT_FOUND, null);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		if (!password.getPassword().equals(password.getConfirmPassword())) {
			response.setErrorResponse(PASSWORD_NOT_MATCH, null);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		String newPassword = MySecurity.hashPassword(password.getPassword());
		newPassword = newPassword.replace("$2a$", "$2y$");

		targetUser.setPassword(newPassword);
		targetUser.setTemporalPassword(false);
		targetUser.setAttempts(0);
		UserEntity resultUser = userRep.save(targetUser);
		if (resultUser == null) {
			response.setErrorResponse(INTERNAL_SERVER_ERROR, null);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.ok();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/get_all", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<OkResponse> getAllUsers(@RequestHeader(Global.X_SESSION) String session) {
		OkResponse response = new OkResponse();
		
		UserEntity user = userRep.findBySession(session);
		if (user == null) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		if (user.getRole() != 0 ) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		Iterable<UserEntity> banks = userRep.findAllByOrderByNameAsc(); 
		if (banks == null) {
			response.data = null;
			response.message = "EMPTY";
			return new ResponseEntity<OkResponse>(response, HttpStatus.NO_CONTENT);
		}
		
		response.data = banks;
		response.message = "OK";
		
		return new ResponseEntity<OkResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get_contacts", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<OkResponse> getMyContacts(@RequestHeader(Global.X_SESSION) String session) {
		OkResponse response = new OkResponse();
		
		UserEntity user = userRep.findBySession(session);
		if (user == null) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		if (user.getRole() != 0 ) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		Iterable<UserEntity> banks = userRep.findAllByOrderByNameAsc(); 
		if (banks == null) {
			response.data = null;
			response.message = "EMPTY";
			return new ResponseEntity<OkResponse>(response, HttpStatus.NO_CONTENT);
		}
		
		response.data = banks;
		response.message = "OK";
		
		return new ResponseEntity<OkResponse>(response, HttpStatus.OK);
	}
	
}
	
