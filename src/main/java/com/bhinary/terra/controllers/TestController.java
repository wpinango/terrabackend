package com.bhinary.terra.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bhinary.terra.response.OkResponse;


@RestController
@RequestMapping(value = "/tests")
public class TestController {
	
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity<OkResponse> test(@RequestHeader Map<String, String> headers, 
			HttpServletResponse responseHeaders) {
		OkResponse response = new OkResponse();
		
		response.setData(false, "OK");
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}

}
