package com.bhinary.terra.controllers;


import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bhinary.terra.entities.GroupEntity;
import com.bhinary.terra.entities.UserEntity;
import com.bhinary.terra.globals.Global;
import com.bhinary.terra.repositories.GroupRepository;
import com.bhinary.terra.repositories.UserRepository;
import com.bhinary.terra.response.BaseResponse;
import com.bhinary.terra.response.OkResponse;
import com.bhinary.terra.util.UserActive;
import com.bhinary.terra.util.UserStatus;
import com.bhinary.terra.util.Validate;

@RestController
@RequestMapping(value = "/groups")
public class GroupController {
	
	@Autowired
	GroupRepository groupRep;
	
	@Autowired
	UserRepository userRep;
	
	@RequestMapping(value = "/get_all", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<OkResponse> getAllGroups(@RequestHeader(Global.X_SESSION) String session) {
		OkResponse response = new OkResponse();
		
		UserEntity user = userRep.findBySession(session);
		if (user == null) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		if (user.getRole() != 0 ) {
			response.setErrorResponse("UNAUTHORIZED", null);
			return new ResponseEntity<OkResponse>(response, HttpStatus.UNAUTHORIZED);
		}
		
		Iterable<GroupEntity> groups = groupRep.findAll(); 
		//GroupEntity groups = groupRep.findById(0);
		if (groups == null) {
			response.data = null;
			response.message = "EMPTY";
			return new ResponseEntity<OkResponse>(response, HttpStatus.NO_CONTENT);
		}
		
		response.data = groups;
		response.message = "OK";
		
		return new ResponseEntity<OkResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json" )
	public ResponseEntity<BaseResponse> registerGroup(@RequestHeader(Global.X_SESSION) String session, @Valid @RequestBody GroupEntity group) {
		OkResponse response = new OkResponse(); 


		//UserEntity result = userRep.save(user);
		
		if (group == null) {
			response.data = null;
			response.message = "EMPTY";
		}
		group.setActive(UserActive.ACTIVE);
		groupRep.save(group);
		response.setOkResponse("OK");

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, produces = "application/json" )
	public ResponseEntity<BaseResponse> editGroup(@RequestHeader(Global.X_SESSION) String session, @Valid @RequestBody GroupEntity group) {
		OkResponse response = new OkResponse(); 


		//UserEntity result = userRep.save(user);

		
		if (group == null) {
			response.setErrorResponse("EMPTY", null);
			return new ResponseEntity<BaseResponse>(response, HttpStatus.BAD_REQUEST);
		}
		
		GroupEntity g = groupRep.findById(group.getId());
		System.out.println("valores : " + g.getGroupName());
		
		g.setGroupName(group.getGroupName());
		g.setGroupType(group.getGroupType());
		g.setId(group.getId());
		g.setActive(group.getActive()); 
		
		//TODO inabilitar todos los usuarios cuando desactives un grupo 
		
		groupRep.save(g);
		
		response.setOkResponse("OK");

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json" )
	public ResponseEntity<BaseResponse> deleteGroup(@RequestHeader(Global.X_SESSION) String session, @Valid @RequestBody GroupEntity group) {
		OkResponse response = new OkResponse(); 


		//UserEntity result = userRep.save(user);

		
		if (group == null) {
			response.data = null;
			response.message = "EMPTY";
		}
		
		GroupEntity g = groupRep.findById(group.getId());
		
		g.setGroupName(group.getGroupName());
		g.setGroupType(group.getGroupType());
		g.setId(group.getId());
		
		groupRep.delete(g);
		
		response.setOkResponse("OK");

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
}
