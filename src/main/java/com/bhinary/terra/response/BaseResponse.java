package com.bhinary.terra.response;

import java.util.Date;

import com.google.gson.Gson;

public abstract class BaseResponse {
	
	public long timestamp;
	public boolean error;
	public String message;
	public Object data;
	//public List<FieldErrorDTO> fieldErrors;
	public Object other;
	
	public BaseResponse() {
		this.initParameters();
	}

	public abstract  void initParameters();
	
	public void setData(boolean error, String message) {
		this.timestamp = new Date().getTime();
		this.error = error;
		this.message = message;
	}

	public String toJSON() {
		Gson gson = new Gson();
		return gson.toJson(this, BaseResponse.class);
	}	
	
	public void ok() {
		this.message = getOK();
		this.error = false;
	}
	
	public void setOkResponse(Object objectResponse) {
		this.data = objectResponse;
		this.message = getOK();
	}
	
	public void setErrorResponse(String errorMessage, Object data) {
		this.data = data;
		this.message = errorMessage;
		this.error = true;
	}
	
	private String getOK() {
		return "OK";
	}
	
	/**
	 * Return a serializes response (json structure inside string)
	 * @return
	 */
	public String toStringResponse() {
		return "";//Global.friendlyGson.toJson(this);		
	}

}
