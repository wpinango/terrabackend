package com.bhinary.terra.models;

import com.google.gson.annotations.SerializedName;

public class AttemptError {

    @SerializedName("attempts") 
	private int attempts;
    
    public void setAttempts(int attempts) {
    	this.attempts = attempts;
    }
    
    public int getAttempts() {
    	return attempts;
    }
}
