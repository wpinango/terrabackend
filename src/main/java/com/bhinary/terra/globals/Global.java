package com.bhinary.terra.globals;

public class Global {

	public static final String VALID_EMAIL_FORMAT = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
	public static final String PASSWORD_REGEXP = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_*@#$%^&+=\\.\\-])(?=\\S+$).{8,}$";
	public static final int MAX_ATTEMPS = 3;
	public static final String X_IDENTITY = "x-identity";
	public static final String X_DS = "x-ds";
	public static final String X_ROLE = "x-role";
	public static final String X_SESSION = "x-session";
}
